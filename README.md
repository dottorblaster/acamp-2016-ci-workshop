# AtlasCamp 2016 Dockerize it Workshop Repository

This is the playground for the AtlasCamp workshop on Docker and CI.
The sample application is a simple [Python], [Flask] web application with tests.

[Flask]: http://flask.pocoo.org/
[Python]: https://www.python.org/

Authors:

- Ian Buchanan
- Nicola Paolucci
